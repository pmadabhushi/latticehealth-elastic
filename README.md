# Elastic
Elasticsearch source code

## Overview
There are two parts to our logging system:
- the ELK server which consists of ElasticSearch (which stores indexed data), Logstash (which indexes data), and Kibana (which allows users to view the data)
- Beats which are modules that can run on the client system, in this case a Raspberry Pi and feed data to either Logstash or directly indexed into ElasticSearch

All of the pieces of ElasticSearch, i.e. the ELK pieces and Beats, can be installed on most platforms including windows, x86, i386 et. al.  However, they don't have a built copy for the Raspberry Pi. This repository contains all of the Beats source code so you can compile beats for the Raspberry Pi platform.

## Beats overview
Beats are modules that run on the client and upload log data to the ELK server. There are several different pre-made beats that exist (ex: packetbeat which uploads network data), or you can develop your own. We are focused on filebeat, which crawls specified files and uploads any new data that exists.

The beats can be configured to upload the data to Logstash via the Lumberjack protocol, which will take care of the indexing and push into elasticsearch.  Or they can be configured to index the data themselves and upload the logs directly to elasticsearch via HTTP.  Our architecture will use the ladder in order to interoperate with Aptible.

## Compiling

### Install GO
GO is a programming language that the beats source code is written in known as GoLang.  There are two steps in this process, installing go1.4 to baseline, then installing go1.7 piggybacking off of the 1.4 installation.

<b>Make and Install Go 1.4</b>
<ul><li>which go && sudo rm -rf /usr/bin/go* /usr/local/go</li>
<li>cd ~</li>
<li>wget https://storage.googleapis.com/golang/go1.4.2.src.tar.gz</li>
<li>sudo tar -C /usr/local -xvf go1.4.*.tar.gz</li>
<li>cd /usr/local/go/src</li>
<li>sudo ./make.bash</li></ul>

That will take a while to build. Once it’s complete download and install Go 1.7 (grabbing the latest version, as before):

<b>Make and Install Go 1.7</b>
<ul><li>cd ~</li>
<li>wget https://storage.googleapis.com/golang/go1.7.src.tar.gz</li>
<li>sudo tar -xvf go1.7.src.tar.gz</li>
<li>sudo mv go /usr/local/go1.7</li>
<li>cd /usr/local/go1.7/src</li>
<li>sudo GOROOT_BOOTSTRAP=/usr/local/go ./make.bash</li>
<li>sudo ln -s /usr/local/go1.7/bin/* /usr/bin</li></ul>

### Clone this repository
<ul><li>cd to your home directory or base directory of your LHS software</li>
<li>git clone https://github.com/LatticeHealth/Elastic</li></ul>

### Building and Installation
Set environment variables in your profile so that they'll be loaded every time. $ nano ~/.profile, then add the lines:<br><br>
PATH=$PATH:/usr/local/go1.7/bin<br>
GOROOT=/usr/local/go1.7/<br>
GOPATH=/home/admin/Elastic/<br><br>
Where /home/admin/Elastic is the path to your repository.

There is a Makefile in the repository root that allows for building and installation (NOTE: don't use make all, just make)
$ cd ~/Elastic
$ make
$ sudo make install

Now build filebeat by navigating to ~/Elastic/src/github.com/elastic/beats/filebeat then running $ sudo make

When compilation completes
sudo cp filebeat /usr/bin/ # can also use "sudo ln -s filebeat /usr/bin" if you plan on keeping the sources around

Rebooting your Raspberry Pi should allow you to use filebeat as a command

### Configuring


