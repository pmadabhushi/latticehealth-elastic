
FILEBEATDIR=src/github.com/elastic/beats/filebeat
SUBDIRS=$(FILEBEATDIR)
IGNORECMDS=install uninstall $(SUBDIRS)

.PHONY: $(SUBDIRS)

all: $(SUBDIRS)

$(filter-out $(IGNORECMDS),$(MAKECMDGOALS)): $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

install:
	# copy opt/lhs to both /opt/lhs and /
	cp -dr --preserve=mode --no-preserve=ownership $(wildcard opt/elastic/*) /
	install -p --mode=0755 $(FILEBEATDIR)/filebeat /usr/bin

uninstall:
	# Remove config files
	rm -rf /etc/filebeat
	# Remove registry
	rm -rf /var/lib/filebeat
	# Remove filebeat upstart and executable
	rm -f /etc/init/filebeat.conf
	rm -f /usr/bin/filebeat

manifest:
	echo $(addprefix /etc/filebeat/, $(notdir opt/elastic/etc/filebeat/*)) | tr ' ' '\n' >>$(MANIFEST)
	echo "/etc/init/filebeat.conf" >>$(MANIFEST)
	echo "/usr/bin/filebeat" >>$(MANIFEST)
